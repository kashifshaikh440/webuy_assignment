<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserCart;
use Auth;

class UserCartController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('cart',['items'=>UserCart::where('user_id',Auth::user()->id)->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_product_exists  = UserCart::where('user_id',Auth::user()->id)->where('product_id',$request->product_id)->first();
        if(!empty($user_product_exists)){
            $user_product_exists->qty = $user_product_exists->qty + 1;
            $user_product_exists->save();
        }
        else{
            $user_cart = new UserCart;
            $user_cart->user_id = Auth::user()->id;
            $user_cart->product_id = $request->product_id;
            $user_cart->qty = $request->qty;
            $user_cart->save();
        }
        return redirect()->route('products.all')->with('success','Product Added to Cart');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_product_exists  = UserCart::where('user_id',Auth::user()->id)->where('product_id',$request->product_id)->first();
        $user_product_exists->qty = $user_product_exists->qty + 1;
        $user_product_exists->save();
        return response()->json(['success'=>'true']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = UserCart::where('id',$id)->first();
        $item->delete();
        return response()->json(['success'=>'true']);
    }

    public function checkout(){
        $item = UserCart::where('user_id',Auth::user()->id)->delete();
        return redirect()->route('products.all')->with('success','Order Placed Succesfully');
    }
}
