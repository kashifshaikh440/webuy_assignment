@extends('layouts.admin')
@section('title')
    Add Category
@endsection
@section('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
@endsection
@section('content')
<div class="container" style="margin-top: 50px;">
    <div class="row justify-content-center">
        <div class="col-md-8">
        	@if(Session::has('success'))
			<p class="alert alert-success">{{ Session::get('success') }}</p>
			@endif
            <div class="card">
                <div class="card-header">{{ __('All Category') }}</div>

                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                    	<thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        	@forelse($categories as $category)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$category->name}}</td>
                                <td style="font-size: 18px;display: flex;">
                                    <a class="btn btn-info"  href="{{route('admin.category.edit',$category->id)}}" style="margin-left: 7px" title="Edit"><i class="fa fa-pencil"></i>Edit</a>
                                    <!-- <div style="margin-left: 10px;">
                                    	<form method="POST" action="{{route('admin.category.destroy',$category->id)}}">
	                                    	@csrf
	                        				@method('DELETE')
	                        				<button class="btn btn-danger" type="submit">Delete</button>
	                                    </form>
                                    </div> -->
                                </td>
                            </tr>
                            @empty
                            <tr>
                            	<td>
                            		No Categories Found
                            	</td>
                            	<td></td>
                            	<td></td>
                            </tr>
                            @endforelse
                        </tbody>    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('style')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
            $('#example1').DataTable();
            $(document).on('click', '.deleteUser', function() {
                var id = $(this).attr('data-id');

                swal({
                    title: "Are you sure?",
                    // text: "But you will still be able to retrieve this file.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '/user/' + id,
                            method: 'post',
                            dataType: 'json',
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "_method": 'DELETE',
                                "id": id,
                            },
                            success: function(response) {
                                swal({
                                    title: "User has been deleted successfully",
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            },
                            error: function(err) {
                                console.log(err);
                            }
                        });
                    } else {
                        swal("Cancelled", "Your User is safe :)", "error");
                    }
                });
            });
        });
    </script>
@endsection
