@extends('layouts.bootstrap')
@section('title')
    All Products
@endsection
@section('content')
<div class="container" style="margin-top: 50px;">
	<div class="container">
	  <div class="row">
	  	<h4>My Cart Items</h4>
	  	<table class="table table-hover">
		  <thead>
		    <tr>
		      <th scope="col">#</th>
		      <th scope="col">Name</th>
		      <th scope="col">Qty</th>
		      <th scope="col">Action</th>
		    </tr>
		  </thead>
		  <tbody>
            @forelse($items as $item)
		    <tr>
		      <th scope="row">{{$loop->iteration}}</th>
		      <td>{{$item->product->title}}</td>
		      <td><input type="number" class="qty" data-id="{{$item->id}}" data-product-id="{{$item->product_id}}" name="qty" value="{{$item->qty}}" style="width: 50px;text-align: center;"> </td>
		      <td style="color: red;font-weight:bold;" data-id="{{$item->id}}" id="remove">X</td>
		    </tr>
            @empty
            <tr>
                <td>No Items in your Cart</td>
            </tr>
            @endforelse
		  </tbody>
		</table>
        @if(count($items) > 0)
		<a href="{{route('checkout')}}" class="btn btn-info" style="color: white;">Checkout</a>
        @endif
        <a href="{{route('products.all')}}" class="btn btn-secondary" style="color: white;margin-left: 20px;">Continue Shopping</a>
	  </div>
	</div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
    	$(document).ready(function() {
            $(document).on('click', '#remove', function() {
                var id = $(this).attr('data-id');
                $(this).closest("tr").remove();
                $.ajax({
                    url:'/cart/'+id,
                    type:'POST',
                    dataType:'json',
                    data:{
                        _token:"{{csrf_token()}}",
                        _method:'DELETE',
                    },
                    success:function(res){
                        alert('Item Removed From Cart');
                    }
                    ,error:function(err){
                        alert('Could not delete item');
                    }
                });
            });

            $(document).on('change','.qty',function(){
                var id = $(this).attr('data-id');
                var qty = $(this).val();
                var product_id = $(this).attr('data-product-id');
                $.ajax({
                    url:'/cart/'+id,
                    type:'POST',
                    dataType:'json',
                    data:{
                        _token:"{{csrf_token()}}",
                        _method:'PUT',
                        product_id:product_id,
                        qty:qty
                    },
                    success:function(res){
                        alert('Quantity Updated Successfully');
                    }
                    ,error:function(err){
                        alert('Could not Update Quantity');
                    }
                });
            })
        });
    </script>
@endsection
