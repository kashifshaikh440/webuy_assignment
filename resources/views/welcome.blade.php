<!DOCTYPE html>
<html>
<head>
    <title>Webuy Assignment</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
    <div class="container" style="margin-top: 50px;">
        <div class="row justify-content-center">
            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('images/admin_1.png')}}" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title text-center">ADMIN</h5>
                <p class="card-text">Visit the admin panel to add range of products</p>
                <a href="#" class="btn btn-primary">Explore</a>
              </div>
            </div>
            <div class="card" style="width: 18rem;">
              <img class="card-img-top" src="{{asset('images/user_3.png')}}" alt="Card image cap">
              <div class="card-body">
                <h5 class="card-title">USER</h5>
                <p class="card-text">Start exlporing the website & checkout new & exiting products.</p>
                <a href="#" class="btn btn-primary">Explore</a>
              </div>
            </div>
        </div>
    </div>
</body>
<footer>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</footer>
</html>