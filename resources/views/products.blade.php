@extends('layouts.bootstrap')
@section('title')
    All Products
@endsection
@section('content')
<div class="container" style="margin-top: 50px;">
	<div class="container">
		@if(Session::has('success'))
			<p class="alert alert-success">{{ Session::get('success') }}</p>
			@endif
	  <div class="row">
	  	@forelse($products as $product)
	    <div class="col-sm">
	      <div class="card" style="width: 18rem;">
			  <img class="card-img-top" src="{{asset('images/'.$product->image)}}" alt="Card image cap" style="max-height: 300px;min-height: 300px;">
			  <div class="card-body">
			    <h5 class="card-title">{{$product->title}}</h5>
			    <p class="card-text">{{$product->description}}</p>
			    <p class="card-text">Rs {{$product->price}}</p>
			    <form method="POST" action="{{route('cart.store')}}">
			    	@csrf
			    	<input type="hidden" name="product_id" value="{{$product->id}}">
			    	<input type="hidden" name="qty" value="1">
			    	<button type="submit" class="btn btn-primary btn-block">Add to Cart</button>
			    </form>
			  </div>
			</div>
	    </div>
        @empty
        <tr>
            <td>No Products available. Please login in as Admin and add products.</td>
        </tr>
	    @endforelse
	  </div>
	</div>
</div>
@endsection

@section('style')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
            $('#example1').DataTable();
            $(document).on('click', '.deleteUser', function() {
                var id = $(this).attr('data-id');

                swal({
                    title: "Are you sure?",
                    // text: "But you will still be able to retrieve this file.",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, Delete it!",
                    cancelButtonText: "No, cancel please!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                }, function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '/user/' + id,
                            method: 'post',
                            dataType: 'json',
                            data: {
                                "_token": "{{ csrf_token() }}",
                                "_method": 'DELETE',
                                "id": id,
                            },
                            success: function(response) {
                                swal({
                                    title: "User has been deleted successfully",
                                    type: "success"
                                }, function() {
                                    location.reload();
                                });
                            },
                            error: function(err) {
                                console.log(err);
                            }
                        });
                    } else {
                        swal("Cancelled", "Your User is safe :)", "error");
                    }
                });
            });
        });
    </script>
@endsection
