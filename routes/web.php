<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

// User Routes...
Route::get('/', 'HomeController@allProducts')->name('products.all');
Route::get('/home', 'HomeController@index')->name('home');
Route::resource('cart', 'UserCartController');
Route::get('checkout', 'UserCartController@checkout')->name('checkout');
// User Routes End...


// ....Admin Routes Start....
Route::redirect('admin','admin/login');
Route::prefix('admin')->name('admin.')->group(function () {
    Route::get('login', 'Auth\Admin\LoginController@showLoginForm')->name('login');
    Route::get('register', 'Auth\Admin\RegisterController@showRegisterForm')->name('register');
    Route::post('register', 'Auth\Admin\RegisterController@register')->name('register.post');
    Route::post('login', 'Auth\Admin\LoginController@login')->name('login.post');
    Route::post('logout', 'Auth\Admin\LoginController@logout')->name('logout');
    Route::resource('category', 'CategoryController');
    Route::resource('product', 'ProductController');
});
// ....Admin Routes ENd....
